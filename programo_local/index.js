const funciones = require('../programo_local/librerias/mis_funciones');
const archivos_list = require('../programo_local/librerias/list_file');
const crear_archivo = require('../programo_local/librerias/crear_archivo');
const leer_archivo = require('../programo_local/librerias/leer_archivos');

//operaciones

let suma = funciones.sumar(1, 2);
console.log('El resultado de la suma es: ', suma);

if(isNaN(suma) === false){
    console.log('El resultado de la suma es'+suma,funciones.msg_ok);
}else{
    console.log(suma,funciones.msg_fail);
};

let resta = funciones.restar(1, 2);
console.log('El resultado de la resta es: ', resta);

if(isNaN(resta) === false){
    console.log('El resultado de la resta es'+resta,funciones.msg_ok);
}else{
    console.log(resta,funciones.msg_fail);
};

let multiplicacion = funciones.multiplicar(1, 2);
console.log('El resultado de la multiplicacion es: ', multiplicacion);

if(isNaN(multiplicacion) === false){
    console.log('El resultado de la multiplicacion es'+multiplicacion,funciones.msg_ok);
}else{
    console.log(multiplicacion,funciones.msg_fail);
};

let divide = funciones.dividir(1, 2);
console.log('El resultado de la divide es: ', divide);

if(isNaN(divide) === false){
    console.log('El resultado de la divide es'+divide,funciones.msg_ok);
}else{
    console.log(divide,funciones.msg_fail);
};

//Leer el listado de archivos en una carpeta

archivos_list.mostrar_archivos(); 

//Generar archivo a partir de los hobbies

const hobbies = ['Batería','Fútbol','Basket','cocinar'];

hobbies.forEach(item => {
    crear_archivo.crear_archivo_plano("log.txt",item);
});

//Leer el archivo

var path_file = process.cwd() + "\\"+ "log.txt";


console.log('path archivo',path_file);
let data = leer_fs.leer_archivo(path_file);
console.log(data);