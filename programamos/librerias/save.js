const fs = require('fs');

function save_file(file, text){
    fs.appendFile(file, text + "\n", function(err){
        if(err){
            return console.log(err);
        }
    });
}

exports.save_file = save_file;