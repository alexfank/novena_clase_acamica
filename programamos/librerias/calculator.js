function sumar(n1, n2){
    if(isNaN(n1) || isNaN(n2)){
        console.log('Hubo un error. Chequee los valores a sumar.')
        return;
    }else{
    return `${n1}+${n2}=${n1+n2}`;
}
};

function restar(n1, n2){
    if(isNaN(n1) || isNaN(n2)){
        console.log('Hubo un error. Chequee los valores a restar.')
        return;
    }else{
    return `${n1}-${n2}=${n1-n2}`;
};
};

function multiplicar(n1, n2){
    if(isNaN(n1) || isNaN(n2)){
        console.log('Hubo un error. Chequee los valores a multiplicar.')
        return;
    }else{
    return `${n1}*${n2}=${n1*n2}`;
};
};

function dividir(n1, n2){
    if(n2 == 0){
        return console.log('No se puede realizar una divición por 0');
    }
    if(isNaN(n1) || isNaN(n2)){
        console.log('Hubo un error. Chequee los valores a dividir.')
        return;
    }
    return `${n1}/${n2}=${n1/n2}`;
};

exports.sumar = sumar;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;